# Simple enumerator classes for easier model handling

Simple package for providing basic enumerators.

## Instalation

Run following composer command.

```shell
composer require dense/enum
```
