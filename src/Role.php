<?php

namespace Dense\Enum;

use Dense\Enum\Core\EnumAbstract;

class Role extends EnumAbstract
{
    const ROLE_ADMIN = 'ADMIN';
    const ROLE_REGULAR = 'REGULAR';

    /**
     * @return array
     */
    public static function getEnums()
    {
        return [
            self::ROLE_ADMIN => 'Admin',
            self::ROLE_REGULAR => 'Regular user',
        ];
    }
}
