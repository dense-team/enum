<?php

namespace Dense\Enum;

use Dense\Enum\Core\EnumAbstract;

class Status extends EnumAbstract
{
    const STATUS_ACTIVE = 'ACTIVE';
    const STATUS_INACTIVE = 'INACTIVE';

    /**
     * @return array
     */
    public static function getEnums()
    {
        return [
            self::STATUS_ACTIVE => 'Active',
            self::STATUS_INACTIVE => 'Inactive',
        ];
    }
}
