<?php

namespace Dense\Enum;

trait HasStatus
{
    /**
     * @var string
     */
    public $status;

    /**
     * @param array $data
     * @return $this
     */
    public function hydrate(array $data)
    {
        if (array_key_exists('status', $data)) {
            $this->status = trim($data['status']);
        }

        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'status' => $this->status,
        ];
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->status === Status::STATUS_ACTIVE;
    }

    /**
     * @return bool
     */
    public function isInactive()
    {
        return $this->status === Status::STATUS_INACTIVE;
    }
}
