<?php

namespace Dense\Enum\Core;

interface EnumInterface
{
    /**
     * @param string $id
     * @return bool
     */
    public static function hasEnum($id);

    /**
     * @return array
     */
    public static function getEnums();

    /**
     * @param string $id
     * @return string
     */
    public static function getEnum($id);
}
