<?php

namespace Dense\Enum\Core;

abstract class EnumAbstract implements EnumInterface
{
    /**
     * @param string $id
     * @return bool
     */
    public static function hasEnum($id)
    {
        $enums = static::getEnums();

        return array_key_exists($id, $enums);
    }

    /**
     * @param string $id
     * @return string
     */
    public static function getEnum($id)
    {
        if (static::hasEnum($id)) {
            $enums = static::getEnums();

            return $enums[$id];
        }

        return $id;
    }

    /**
     * @return array
     */
    public static function getEnumProps()
    {
        return array_keys(static::getEnums());
    }

    /**
     * @return array
     */
    public static function getEnumValues()
    {
        return array_values(static::getEnums());
    }
}
